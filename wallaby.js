module.exports = function (wallaby) {
    return {
        files: [
            {pattern: 'node_modules/phantomjs-polyfill/bind-polyfill.js', instrument: false},
            {pattern: 'node_modules/es6-shim/es6-shim.js', instrument: false},
            {pattern: 'node_modules/systemjs/dist/system-polyfills.js', instrument: false},
            {pattern: 'node_modules/systemjs/dist/system.js', instrument: false},
            {pattern: 'node_modules/reflect-metadata/Reflect.js', instrument: false},
            {pattern: 'app/**.ts', load: false},
            {pattern: 'app/specs/*.spec.ts', ignore: true}
        ],
        tests: [
            {pattern: 'app/specs/*.spec.ts', load: false}
        ],
        compilers: {
            'app/**/*.ts': wallaby.compilers.typeScript({
                "module": 4, // <--- 2 for AMD, or 4 for System.js
                "emitDecoratorMetadata": true,
                "experimentalDecorators": true,
                "noImplicitAny": false
            })
        },
        middleware: function (app, express) {
            app.use('/node_modules',
                express.static(
                    require('path').join(__dirname, 'node_modules')));
        },
        bootstrap: function (wallaby) {
            wallaby.delayStart();

            System.config(
                {
                    defaultJSExtensions: true,
                    packages: {
                        app: {
                            // for inline errors
                            meta:
                            {
                                '*': {
                                    scriptLoad: true
                                }
                            }
                        }
                    }
                    ,
                    paths: {
                        "npm:*": "node_modules/*"
                    },
                    map: {
                        "angular2": "npm:angular2",
                        "rxjs": "npm:rxjs"
                    }
                }
            );

            var promises = [];
            for (var i = 0, len = wallaby.tests.length; i < len; i++) {
                promises.push(System['import'](wallaby.tests[i].replace(/\.js$/, '')));
            }

            Promise.all(promises).then(function () {
                wallaby.start();
            });
        }
    };
};
